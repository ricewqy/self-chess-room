package cn.chess.service;

import cn.chess.domain.Boxes;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author zgc
* @description 针对表【boxes(包厢表)】的数据库操作Service
* @createDate 2023-03-27 10:31:59
*/
public interface BoxesService  {

    List<Boxes> getAll();

    List<Boxes> page(Integer pa, Integer ps);

    String[] checkTime(String nowDate,String bid);

    Boxes getById(Integer id);
}
