package cn.chess.service.impl;

import cn.chess.domain.Boxes;
import cn.chess.domain.Orders;
import cn.chess.mapper.BoxesMapper;
import cn.chess.mapper.OrdersMapper;
import cn.chess.service.BoxesService;
import cn.chess.util.DateTimeUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author zgc
 * @description 针对表【boxes(包厢表)】的数据库操作Service实现
 * @createDate 2023-03-27 10:31:59
 */
@Service
public class BoxesServiceImpl implements BoxesService {
    @Autowired
    private BoxesMapper boxesMapper;
    @Autowired
    private OrdersMapper orderMapper;

    @Override
    public List<Boxes> getAll() {

        return boxesMapper.selectList(null);
    }

    @Override
    public Boxes getById(Integer id) {

        return boxesMapper.selectById(id);
    }

    @Override
    public String[] checkTime(String nowDate,String bid) {
        //        转换日期
        Date startTime = DateTimeUtils.convertString2String("yyyy-MM-dd", nowDate);
        Date endTime = DateTimeUtils.dateAdd(startTime, 1);
        //    根据时间查询
        QueryWrapper<Orders> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .and(wapper -> wapper.ge("startTime", startTime))
                .and(wapper -> wapper.le("expiryTime", endTime))
                .and(wapper -> wapper.eq("bid",bid));
        List<Orders> ordersList = orderMapper.selectList(queryWrapper);
        List<String[]> arrList = new ArrayList<>();
        for (Orders o : ordersList) {
            arrList.add(o.getTimes());
        }

        return s21(arrList);
    }

    @Override
    public List<Boxes> page(Integer pa, Integer ps) {
        Page<Boxes> page = boxesMapper.selectPage(new Page<>(pa, ps), null);
        return page.getRecords();
    }

    //    合并数组去重
    public String[] s21(List<String[]> arrList) {
        Map<String, String> map = new TreeMap<>();
        int j = 0;
        arrList.forEach((item) -> {
            for (int i = 0; i < item.length; i++) {
                map.put(item[i], item[i]);
            }

        });
        Collection<String> values = map.values();
        Iterator<String> iterator = values.iterator();
        String[] c = new String[values.size()];
        while (iterator.hasNext()) {
            c[j++] = iterator.next();
        }
        return c;

    }
}




