package cn.chess.service;

import cn.chess.domain.User;
import cn.chess.vo.WxUserVo;

/**
 * @author zgc
 * @description 针对表【user(用户表)】的数据库操作Service
 * @createDate 2023-03-27 10:32:16
 */
public interface UserService {
    //  根据用户id查询订单信息
    User getUserById(Integer userid);

    // 根据手机号查询 用户信息
    User getUserByPhone(String phone);
    int
     update(User user);

    WxUserVo getWxUser(String code);

    void initUser(String user);

    User getUserByOpenid(String openid);
}
