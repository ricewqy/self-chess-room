package cn.chess.service;

import cn.chess.domain.Orders;
import cn.chess.vo.OrdersVO;
import cn.chess.vo.PayOrderVO;

import java.util.List;

/**
 * @author zgc
 * @description 针对表【order(订单表)】的数据库操作Service
 * @createDate 2023-03-27 10:32:08
 */
public interface OrderService {

    List<Orders> getOrderByUserID(Integer id);

    // 根据用户id查询可用 订单信息   用于开大门   true  有可用订单  false  无可用订单
    Boolean determineOpen(String openid);

    // 用户预约下单
    Orders places(OrdersVO ordersVO);

    void updataPayStatus(String orderId, int payStatus);

    //  订单支付  ，优先扣余额
//      0 余额不足  1 支付成功  2 已经支付过了  3.信息错误
    Integer payOrder(PayOrderVO payOrderVO);

    Orders getOrderByOrderid(String orderid);

    List<Orders> getOrderList(String openid,Integer type, Integer page);
}
