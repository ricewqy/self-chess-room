package cn.chess.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: springBoot_film
 * @BelongsPackage: com.film.springboot_film.config
 * @Author: zhou
 * @CreateTime: 2022-11-08  16:48
 * @Description: TODO
 * @Version: 1.0
 */
@Configuration
public class MybatisPlusConfig {
    /**
     *
     * @return MybatisPlusInterceptor 配置mybatisPlus拦截器 、配置好后mybatisPlus的分页才能用
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

   /* @Bean
    public ConfigurationCustomizer configurationCustomizer(){
        return new ConfigurationCustomizer() {
            @Override
            public void customize(org.apache.ibatis.session.Configuration configuration) {
//                开启驼峰命名规则，可以将数据库中的下划线映射为驼峰命名
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };
    }*/
}
