package cn.chess.vo;

import lombok.Data;

@Data
public class WxUserVo {
    private String openid;
    private String session_key;
    private String unionid;
    private String errcode;


    private String errmsg;


}
