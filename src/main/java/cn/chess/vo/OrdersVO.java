package cn.chess.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单表
 *
 * @TableName order
 */
//@TableName(value = "order")
@Data
public class OrdersVO implements Serializable {
//    /**
//     * 订单id
//     */
//    @TableId(type = IdType.AUTO)
//    private Integer oid;

    /**
     * 用户下单手机
     */
    private String phone;
    /**
     * 预约时间
     */
    private String selectTime;

    /**
     * 订单详情
     */
    private String details;


    /**
     * 订单金额
     */
    private BigDecimal price;
    /**
     * 用户的openid
     */
    private String openid;

    /**
     * 用户id
     */
    private Integer uid;

    /**
     * 包厢id
     */
    private Integer bid;
    /**
     * 订单开始时间
     **/
    private Long startTime;
    /**
     * 订单结束时间
     */
    private Long expiryTime;
}
