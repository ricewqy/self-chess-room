package cn.chess.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 包厢表
 *
 * @TableName boxes
 */
@TableName(value = "boxes")
@Data
public class BoxesVO implements Serializable {
    /**
     * 包厢id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 包厢名称
     */
    private String boxname;

    /**
     * 包厢详情
     */
    private String boxdetails;

    /**
     * 包厢价格
     */
    private BigDecimal boxprice;

    /**
     * 包厢状态
     */
    private Integer boxstatus;

    /**
     * 包厢图片路径
     */
    private String pic;
    /**
     * 推荐人数
     */
    private Integer renshu;
}
