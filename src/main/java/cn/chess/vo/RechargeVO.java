package cn.chess.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 充值订单表
 * @TableName recharge
 */
@TableName(value ="recharge")
@Data
public class RechargeVO implements Serializable {
    /**
     * 充值订单id
     */
    @TableId
    private Integer id;

    /**
     * 充值时间
     */
    private Date rechargetime;

    /**
     * 充值金额
     */
    private String amount;

    /**
     * 充值用户
     */
    private Integer uid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
