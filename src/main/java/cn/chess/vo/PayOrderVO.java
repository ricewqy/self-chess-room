package cn.chess.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PayOrderVO {
    private String openid;
    private String orderId;


}
