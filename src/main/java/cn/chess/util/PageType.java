package cn.chess.util;

public class PageType {
    //    分页每页数量
    public static final Integer PAGE_SIZE = 10;
    //    1  未支付
    public static final Integer TYPE_NOPAY = 1;
    //   2 以预约
    public static final Integer TYPE_MAKE = 2;
    //    3 已使用
    public static final Integer TYPE_USED = 3;
    //    4 已过期
    public static final Integer TYPE_STALE = 4;
    //   订单过期时间 分钟
    public static final Integer STALE_TIME = -30;

}
