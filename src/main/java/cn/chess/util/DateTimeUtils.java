package cn.chess.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtils {
    public static Date convertLong2Date(String format, Long dateLong) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {

            Date date = new Date(dateLong);
            return date;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertDate2String(String format, Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }


    public static void main(String[] args) {
//        System.out.println(convertString2Date("HH:mm:ss", "19:00:00"));
    }


    public static Date convertString2String(String format, String nowDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

        Date date = null;
        try {
            date = simpleDateFormat.parse(nowDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Date dateAdd(Date nowDate, int days) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(nowDate);
        // 把日期往后增加一天,整数  往后推,负数往前移动
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }
    public static Date dateAddMinute(Date nowDate, int minute) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(nowDate);
        // 把日期往后增加一天,整数  往后推,负数往前移动
        calendar.add(Calendar.MINUTE, minute);
        return calendar.getTime();
    }


    public static long dateMinute(Date beginDate, Date endDate) {

        long begin = beginDate.getTime();
        long end = endDate.getTime();
        long timeLag = end - begin;
        //天
        long day = timeLag / (24 * 60 * 60 * 1000);
        //小时
        long hour = (timeLag / (60 * 60 * 1000) - day * 24);
        //分钟
        long minute = ((timeLag / (60 * 1000)) - day * 24 * 60 - hour * 60);
        return minute;
    }
}
