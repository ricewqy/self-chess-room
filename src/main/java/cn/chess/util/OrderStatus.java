package cn.chess.util;

public class OrderStatus {
    //    订单支付成功
    public static final int PAY_SUCCESS = 1;
    //    点单支付失败
    public static final int PAY_DEFEATED = 0;
    //    订单已使用
    public static final int ORDER_STATUS_USED = 1;
    //    订单未使用
    public static final int ORDER_STATUS_UNUSED = 0;
}
