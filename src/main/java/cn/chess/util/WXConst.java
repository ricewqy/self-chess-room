package cn.chess.util;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component

@Data
public class WXConst {


    public static String grant_type = "authorization_code";
    //微信小程序appid
    public static String appId = "wx15e7b94542cbb126";
    //微信小程序appsecret
    public static String appSecret = "f9b32721ea7d86976b487530aceb8d78";
    //微信支付主体
    public static String title = "预约棋牌室";
    public static String orderNo = "";
    //微信商户号
    public static String mch_id = "1642078615";
    //微信支付的商户密钥
    public static final String key = "DIyGFN0vXtGAoOeUldHotOuLNkUAiapo";
    //获取微信Openid的请求地址
    public static String WxGetOpenIdUrl = "https://api.weixin.qq.com/sns/jscode2session?";
    //支付成功后的服务器回调url
    public static final String notify_url = "https://api.weixin.qq.com/sns/jscode2session";
    //签名方式
    public static final String SIGNTYPE = "MD5";
    //交易类型
    public static final String TRADETYPE = "JSAPI";
    //微信统一下单接口地址
    public static final String pay_url = "https://api.mch.weixin.qq.com/pay/unifiedorder";

}
