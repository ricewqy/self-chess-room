package cn.chess.mapper;

import cn.chess.domain.Boxes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author zgc
* @description 针对表【boxes(包厢表)】的数据库操作Mapper
* @createDate 2023-03-27 10:31:59
* @Entity generator.domain.Boxes
*/
@Mapper
public interface BoxesMapper extends BaseMapper<Boxes> {

}




