package cn.chess.mapper;

import cn.chess.domain.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author zgc
* @description 针对表【order(订单表)】的数据库操作Mapper
* @createDate 2023-03-27 10:32:08
* @Entity generator.domain.Orders
*/
@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {

}




