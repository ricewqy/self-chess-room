package cn.chess.mapper;

import cn.chess.domain.Recharge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author zgc
* @description 针对表【recharge(充值订单表)】的数据库操作Mapper
* @createDate 2023-03-28 10:30:36
* @Entity cn.chess.domain.Recharge
*/
@Mapper
public interface RechargeMapper extends BaseMapper<Recharge> {

}




