package cn.chess.mapper;

import cn.chess.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author zgc
* @description 针对表【user(用户表)】的数据库操作Mapper
* @createDate 2023-03-27 10:32:16
* @Entity generator.domain.UserVO
*/
@Mapper
public interface UserMapper extends BaseMapper<User> {

}




