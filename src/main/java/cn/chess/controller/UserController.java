package cn.chess.controller;

import cn.chess.domain.Orders;
import cn.chess.domain.User;
import cn.chess.service.OrderService;
import cn.chess.service.UserService;
import cn.chess.vo.WxUserVo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);//cn.chess.controller.UserController

    //Logger logger = (Logger) LogManager.getLogger(UserController.class.getName());

    @Autowired
    OrderService orderService;
    @Autowired
    UserService userService;


    @GetMapping("/get")
    public String get() {

        logger.trace("trace...");
        logger.debug("debug...");
        logger.info("info...");
        logger.warn("warn...");
        logger.error("error...");
        return "OK";
    }

    //  根据用户id查询订单信息
    @GetMapping("/getOrder/{id}")
    public List<Orders> getOrderByUserID(@PathVariable Integer id) {
        List<Orders> orderList = orderService.getOrderByUserID(id);
        log.info("用户的订单信息为:{}", orderList);
        return orderList;
    }

    //  根据用户id查询用户信息

    @GetMapping("/{userid}")
    public User getUser(@PathVariable Integer userid) {
        User user = userService.getUserById(userid);
        if (user == null) {
            logger.error("查无此人");
        }
        return user;

    }

    //    根据手机号查询用户信息
    @GetMapping("/phone/{phone}")
    public User getPhoneUser(@PathVariable String phone) {
        return userService.getUserByPhone(phone);
    }

    // TODO 登录
    @GetMapping("/login")
    public WxUserVo login(String code) {

        return userService.getWxUser(code);

    }


}
