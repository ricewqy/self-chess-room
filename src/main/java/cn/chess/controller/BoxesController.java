package cn.chess.controller;

import cn.chess.domain.Boxes;
import cn.chess.service.BoxesService;
import cn.chess.service.OrderService;
import cn.chess.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @BelongsProject: springboot_logback_demo
 * @BelongsPackage: cn.chess.controller
 * @Author: zhou
 * @CreateTime: 2023-03-27  16:42
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/boxes")
public class BoxesController {
    @Autowired
    private BoxesService boxesService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/checkTime")
    public String[] checkTime(String nowDate,String bid) {

        return boxesService.checkTime(nowDate,bid);
    }

    // 根据房间id查询 房间信息
    @GetMapping("/{id}")
    public Boxes getById(@PathVariable("id") Integer id) {
        Boxes byId = boxesService.getById(id);

        return byId;
    }

    // 分页查询   页码  每页大小
    @GetMapping("/{pa}/{ps}")
    public List<Boxes> pageBoxes(@PathVariable Integer pa, @PathVariable Integer ps) {

        return boxesService.page(pa, ps);
    }
    //　TODO　查询包厢的预约时间段

    // TODO 打开包厢门
    public Boolean openDoor2Room() {

        return null;
    }

    //    开大门
    @PostMapping
    public Boolean openDoor(@RequestBody UserVO userVO) {
        // 1.判断用户是否有可用订单
        Boolean ifOpen = orderService.determineOpen(userVO.getOpenid());

        if (ifOpen) {
            // TODO 2.有可用订单 开门
            return ifOpen;
        }
        return ifOpen;
    }
    //　TODO　修改包厢价格

}
