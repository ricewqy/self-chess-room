package cn.chess.controller;

import cn.chess.domain.Orders;
import cn.chess.service.OrderService;
import cn.chess.service.UserService;
import cn.chess.vo.OrdersVO;
import cn.chess.vo.PayOrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @BelongsProject: springboot_logback_demo
 * @BelongsPackage: cn.chess.controller
 * @Author: zhou
 * @CreateTime: 2023-03-27  16:44
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/orders")
public class OrdersController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

    //根据订单号查询 订单信息
    @GetMapping("/{orderid}")
    public Orders getOrder(@PathVariable("orderid") String orderid) {
        return orderService.getOrderByOrderid(orderid);

    }

    // 用户下单预约功能
    @PostMapping("/places")
    public Orders places(@RequestBody OrdersVO ordersVO) {

        return orderService.places(ordersVO);

    }
    //TODO　统计一天的营业额（查询指定日期的营业额）

    //  订单支付  ，优先扣余额
//      0 未支付  1 支付成功  2 已经支付过了  3.信息错误
    @PostMapping("/pay")
    @Transactional
    public Integer payOrder(@RequestBody PayOrderVO payOrderVO) {

        return orderService.payOrder(payOrderVO);
    }

    @GetMapping("/getOrderList.html")
    public List<Orders> getOrderList(String openid,Integer type, Integer page) {
        return orderService.getOrderList(openid,type, page);
    }
}

