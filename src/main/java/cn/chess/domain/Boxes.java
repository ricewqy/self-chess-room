package cn.chess.domain;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 包厢表
 *
 * @TableName boxes
 */
@JsonIgnoreProperties
@TableName(value = "boxes")
@Data
public class Boxes implements Serializable {
    /**
     * 包厢id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 包厢名称
     */
    private String boxname;

    /**
     * 包厢详情
     */
    private String boxdetails;

    /**
     * 包厢价格
     */
    private BigDecimal boxprice;

    /**
     * 包厢状态
     */
    private Integer boxstatus;
    /**
     * 包厢详情
     */
    private String detail;
    /**
     * 包厢图片路径
     */
    private String pic;

    /**
     * 包厢最小预定时间*/
    private Integer minitime;
    /**
     * 推荐人数
     */
    private Integer renshu;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


    /**
     * 房间介绍
     */
    @TableField(exist = false)
    private String content;

    public String getContent() {
        return "<p>" + this.detail + "</p>";

    }

    public void setContent(String content) {
        this.content = content;
    }


    @TableField(exist = false)
    private String[] pics;

    public String[] getPics() {
        return JSONArray.parseObject(this.pic, String[].class);
    }



    @Override
    public String toString() {
        return "Boxes{" +
                "id=" + id +
                ", boxname='" + boxname + '\'' +
                ", boxdetails='" + boxdetails + '\'' +
                ", boxprice=" + boxprice +
                ", boxstatus=" + boxstatus +
                ", detail='" + detail + '\'' +
                ", pic='" + pic + '\'' +
                ", renshu=" + renshu +
                ", content='" + content + '\'' +
                ", pics=" + Arrays.toString(pics) +
                '}';
    }
}
