package cn.chess.domain;

import cn.chess.util.DateTimeUtils;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static cn.chess.util.PageType.*;

/**
 * 订单表
 *
 * @TableName orders
 */
@TableName(value = "orders")
@Data
public class Orders implements Serializable {
    /**
     * 订单id
     */
    @TableId
    private String oid;
    /**
     * 用户的openid
     */
    private String openid;
    /**
     * 用户下单手机
     */
    private String phone;

    /**
     * 订单创建时间
     */
    @TableField(value = "createTime", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 订单详情
     */
    private String details;

    /**
     * 订单是否已消费(0：未消费 1：已消费)
     */
    private Integer status;
    /**
     * 订单支付状态 (0 : 未支付   1 ： 已支付)
     */
    private Integer paystatus;
    /**
     * 订单金额
     */
    private BigDecimal price;

    /**
     * 用户id
     */
    private Integer uid;

    /**
     * 包厢id
     */
    private Integer bid;
    /**
     * 订单选择的时间
     */
    @TableField(value = "selectTime")

    private String selectTime;
    /**
     * 订单开始时间
     **/
    @TableField(value = "startTime")
    private Date startTime;
    /**
     * 订单结束时间
     */
    @TableField(value = "expiryTime")
    private Date expiryTime;


    @TableField(exist = false)
    private Boxes boxes;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /*
     *服务时长*/
    @TableField(exist = false)
    private Long hours;

    public Long getHours() {
        long diff = this.expiryTime.getTime() - this.startTime.getTime();
//        小时
        long diffHours = diff / (60 * 60 * 1000) % 24;
        hours = diffHours;
        return hours;
    }

    // 服务时间字符串
    @TableField(exist = false)
    private String date;

    public String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("MM月dd日 HH:mm");
        String s = format.format(startTime);
        String e = format.format(expiryTime);
        return s + " - " + e;
    }

    @TableField(exist = false)
    private String[] times;

    /**
     * 订单类型
     * 1  未支付   未过期
     * 2  以预约  未过期  已支付  未使用
     * 3  已使用
     * 4  已过期  创建时间超过30分钟  预约结束时间过期  未使用
     */
    @TableField(exist = false)
    private Integer type;

    public Integer getType() {
        boolean stale = false;  // 是否过期
        boolean pay = paystatus == 0 ? false : true; // 是否支付
        boolean used = status == 0 ? false : true; // 是否使用过
        Date date = new Date();
        long minute = DateTimeUtils.dateMinute(createTime, date);
        //        判断创建时间有没有过期
        if (minute < 30 && (expiryTime.getTime() > date.getTime() && !used)) {
            //没过期
            stale = true;
        }
        if (!pay && stale) {
            return TYPE_NOPAY;
        }
        if (stale && pay && !used) {

            return TYPE_MAKE;
        }
        if (used) {
            return TYPE_USED;
        }
        if (!stale && !used) {
            return TYPE_STALE;
        }
        return 0;
    }

    public String[] getTimes() {

        return JSONArray.parseObject(selectTime, String[].class);
    }

    public void setTimes(String[] times) {
        this.times = JSONArray.parseObject(selectTime, String[].class);
    }

    @Override
    public String toString() {
        return "Orders{" +
                "oid=" + oid +
                ", openid='" + openid + '\'' +
                ", phone='" + phone + '\'' +
                ", createTime=" + createTime +
                ", details='" + details + '\'' +
                ", status=" + status +
                ", paystatus=" + paystatus +
                ", price=" + price +
                ", uid=" + uid +
                ", bid=" + bid +
                ", selectTime='" + selectTime + '\'' +
                ", startTime=" + startTime +
                ", expiryTime=" + expiryTime +
                ", times=" + Arrays.toString(times) +
                '}';
    }
}
