package cn.chess.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户表
 * @TableName user
 */
@TableName(value ="user")
@Data
public class User implements Serializable {
    /**
     * 用户id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户的openid
     */
    private String openid;
    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 用户手机号
     */
    private String phone;

    /**
     * 用户余额
     */
    private BigDecimal balance;

    /**
     * 登入时间
     */
    private Date logintime;

    /**
     * 登出时间
     */
    private Date logouttime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
