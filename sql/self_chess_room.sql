/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : self_chess_room

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 10/04/2023 19:35:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for boxes
-- ----------------------------
DROP TABLE IF EXISTS `boxes`;
CREATE TABLE `boxes`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '包厢id',
  `boxname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '包厢名称',
  `boxdetails` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '包厢详情',
  `boxprice` decimal(10, 2) NOT NULL COMMENT '包厢价格',
  `boxstatus` int(0) NOT NULL COMMENT '包厢状态 （0空闲，1已占用，2待清洁）',
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  `renshu` int(0) NOT NULL COMMENT '推荐人数',
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '包厢描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '包厢表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of boxes
-- ----------------------------
INSERT INTO `boxes` VALUES (1, 'V666【大包】', 'test', 25.00, 0, '[\"/image/cs01.jpg\",\"/image/cs02.jpg\"]', 4, '你好');
INSERT INTO `boxes` VALUES (10, 'v666小包', '测试2', 100.00, 0, '[\"/image/cs01.jpg\",\"/image/cs02.jpg\"]', 5, '');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `oid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单id',
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '微信用户openid',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户下单手机',
  `createTime` datetime(0) NOT NULL COMMENT '订单创建时间',
  `details` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单详情',
  `paystatus` int(0) NOT NULL COMMENT '订单支付状态(0：支付失败 1：支付成功)',
  `price` decimal(10, 2) NOT NULL COMMENT '订单金额',
  `uid` int(0) NULL DEFAULT NULL COMMENT '用户id',
  `bid` int(0) NOT NULL COMMENT '包厢id',
  `status` int(0) NOT NULL COMMENT '订单是否已消费(0 未消费   1 已消费)',
  `selectTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单选择的时间',
  `startTime` datetime(0) NOT NULL COMMENT '订单开始时间',
  `expiryTime` datetime(0) NOT NULL COMMENT '订单结束时间',
  PRIMARY KEY (`oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('CR765534307002351616', 'oL2ka5M8pYOukmLEFTyqaEpltmb4', NULL, '2023-04-10 11:20:02', NULL, 0, 100.00, NULL, 1, 0, '[\"19:30\",\"20:00\",\"20:30\",\"21:00\",\"21:30\",\"22:00\",\"22:30\",\"23:00\",\"23:30\"]', '2023-04-10 11:30:00', '2023-04-10 15:30:00');
INSERT INTO `orders` VALUES ('CR765534394109657088', 'oL2ka5M8pYOukmLEFTyqaEpltmb4', NULL, '2023-04-10 11:20:22', NULL, 0, 100.00, NULL, 1, 0, '[\"19:30\",\"20:00\",\"20:30\",\"21:00\",\"21:30\",\"22:00\",\"22:30\",\"23:00\",\"23:30\"]', '2023-04-10 11:30:00', '2023-04-10 15:30:00');

-- ----------------------------
-- Table structure for recharge
-- ----------------------------
DROP TABLE IF EXISTS `recharge`;
CREATE TABLE `recharge`  (
  `id` int(0) NOT NULL COMMENT '充值订单id',
  `rechargetime` datetime(0) NOT NULL COMMENT '充值时间',
  `amount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '充值金额',
  `uid` int(0) NOT NULL COMMENT '充值用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '充值订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '微信用户的openid',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机号',
  `balance` decimal(10, 2) NOT NULL COMMENT '用户余额',
  `logintime` datetime(0) NULL DEFAULT NULL COMMENT '登入时间',
  `logouttime` datetime(0) NULL DEFAULT NULL COMMENT '登出时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '', 'test01', '18274742262', 400.00, '2023-03-26 00:00:00', '2023-03-27 00:00:00');
INSERT INTO `user` VALUES (2, 'oL2ka5M8pYOukmLEFTyqaEpltmb4', NULL, NULL, 198.00, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
